/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.commons;

import com.google.common.base.Joiner;
import com.google.common.io.BaseEncoding;
import com.google.common.io.CharStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class ObjectManipulation {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( ObjectManipulation.class );

	/**
	 * @param object
	 *
	 * @return
	 */
	public static String serializeObject ( final Object object ) {
		String serialized = "";
		try {
			ByteArrayOutputStream stream = new ByteArrayOutputStream ();
			ObjectOutputStream out = new ObjectOutputStream ( stream );
			out.writeObject ( object );
			out.close ();
			serialized = new String ( stream.toByteArray () );
		} catch ( IOException e ) {
			LOG.debug ( "Error while serializing object", e );
		}
		return serialized;
	}

	/**
	 * @param compress
	 *
	 * @return
	 */
	public static String compressString ( final String compress ) {
		ByteArrayOutputStream baOS = new ByteArrayOutputStream ();
		try {
			GZIPOutputStream gzOS = new GZIPOutputStream ( baOS );
			Writer writer = new OutputStreamWriter ( gzOS );
			writer.write ( compress );
			writer.close ();
			baOS.close ();
		} catch ( IOException e ) {
			LOG.debug ( "Error while compressing String", e );
		}
		return BaseEncoding.base64 ().encode ( baOS.toByteArray () );
	}

	/**
	 * @param decompress
	 *
	 * @return
	 */
	public static String decompressString ( final String decompress ) {
		ByteArrayInputStream baIS = new ByteArrayInputStream ( BaseEncoding.base64 ().decode ( decompress ) );
		String joined = "";
		try {
			GZIPInputStream gzIS = new GZIPInputStream ( baIS );
			Reader reader = new InputStreamReader ( gzIS );
			List<String> lines = CharStreams.readLines ( reader );
			joined = Joiner.on ( "" ).join ( lines );
		} catch ( IOException e ) {
			LOG.debug ( "Error while decompressing String", e );
		}
		return joined;
	}
}
