/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.commons.tests;

import ch.hesso.commons.ObjectManipulation;
import com.google.common.base.Joiner;
import com.google.common.io.Files;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ManipulationTests {

	@Test
	public void testCompressLargeHTMLFile () throws IOException {
		File f = new File ( ManipulationTests.class.getResource ( "/test.html" ).getFile () );
		Assert.assertTrue ( f.exists () );
		List<String> lines = Files.readLines ( f, Charset.defaultCharset () );
		String content = Joiner.on ( "" ).join ( lines );
		String compress = ObjectManipulation.compressString ( content );
		String decompress = ObjectManipulation.decompressString ( compress );
		Assert.assertTrue ( content.getBytes ().length == decompress.getBytes ().length );
	}

	@Test
	public void testHash () throws MalformedURLException {
		URL url = new URL ( "http://dl.acm.org/citation.cfm?id=2207980" );
		//System.out.println ( Math.abs ( url.hashCode () ) % 50 );
		Assert.assertTrue ( Math.abs ( url.hashCode () ) % 50 == 18 );
		url = new URL ( "http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=5946649" );
		Assert.assertTrue ( Math.abs ( url.hashCode () ) % 50 == 11 );
		url = new URL ( "http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=5916649" );
		Assert.assertTrue ( Math.abs ( url.hashCode () ) % 50 == 48 );
	}

}
